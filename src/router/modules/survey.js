import Layout from '@/layout'

const surveyRouter = {
  path: '/survey',
  component: Layout,
  redirect: '/survey/index',
  meta: {
    icon: 'library_books',
    roles: ['admin']
  },
  active: false,
  children: [
    {
      path: 'index',
      component: () => import('@/views/survey/ListSurvey'),
      meta: {
        title: 'survey_title',
        icon: 'library_books',
        noCache: true,
        affix: true,
        roles: ['admin']
      }
    },
    {
      path: 'create',
      component: () => import('@/views/survey/FormBuildSurvey'),
      hidden: true,
      meta: {
        title: 'create_survey',
        roles: ['admin']
      }
    },
    {
      path: 'edit',
      component: () => import('@/views/survey/FormBuildSurvey'),
      hidden: true,
      meta: {
        title: 'edit_survey',
        roles: ['admin']
      }
    },
    {
      path: 'results',
      component: () => import('@/views/survey/ResultsSurvey'),
      hidden: true,
      meta: {
        title: 'results_survey',
        roles: ['admin']
      }
    }
  ]
}

export default surveyRouter
