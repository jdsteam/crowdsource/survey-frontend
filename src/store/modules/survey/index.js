import actions from './actions'
import mutations from './mutations'
import getters from './getters'

function initialState() {
  return {
    listSurvey: null,
    listResponseSurvey: null,
    formSurvey: {
      id: null,
      survey_name: '',
      description: '',
      introduction: '',
      periode_start: '',
      periode_end: '',
      respondent_target: null,
      url_survey_token: ''
    },
    questionsList: [],
    totalPage: 0,
    detailSurvey: null
  }
}

export const state = {
  initialState: initialState(),
  listSurvey: initialState().listSurvey,
  totalPage: initialState().totalPage,
  formSurvey: initialState().formSurvey,
  questionsList: initialState().questionsList,
  detailSurvey: initialState().detailSurvey
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
