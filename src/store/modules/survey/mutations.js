export default {
  SET_LIST_SURVEY: (state, data) => {
    state.listSurvey.push(...data.data)
    state.totalPage = data._metaData.totalPages
  },
  SET_LIST_RESPONSE_SURVEY: (state, data) => {
    state.listResponseSurvey = data.answers
  },
  SET_DATA_SURVEY: (state, data) => {
    state.formSurvey.id = data.id
    state.formSurvey.survey_name = data.survey_name
    state.formSurvey.description = data.description
    state.formSurvey.introduction = data.introduction
    state.formSurvey.periode_start = data.periode_start
    state.formSurvey.periode_end = data.periode_end
    state.formSurvey.respondent_target = data.respondent_target
    state.formSurvey.url_survey_token = data.url
  },
  PUSH_QUESTIONS_ARRAY_SURVEY: (state, data) => {
    state.questionsList.push(data)
  },
  DELETE_QUESTIONS_ARRAY_SURVEY: (state, index) => {
    state.questionsList.splice(index, 1)
  },
  SET_QUESTIONS_SURVEY: (state, data) => {
    state.questionsList = data
  },
  SET_FORM_SURVEY_DESCRIPTION: (state, data) => {
    state.formSurvey.description = data
  },
  SET_FORM_SURVEY_INTRODUCTION: (state, data) => {
    state.formSurvey.introduction = data
  },
  SET_DETAIL_SURVEY: (state, data) => {
    state.detailSurvey = data
  },
  RESET_FORM_SURVEY: (state) => {
    state.formSurvey.id = null
    state.formSurvey.survey_name = ''
    state.formSurvey.description = ''
    state.formSurvey.introduction = ''
    state.formSurvey.periode_start = ''
    state.formSurvey.periode_end = ''
    state.formSurvey.respondent_target = null
    state.formSurvey.url_survey_token = ''
    state.questionsList = []
  },
  RESET_DETAIL_SURVEY: (state) => {
    state.detailSurvey = null
  },
  RESET_LIST_SURVEY: (state) => {
    state.listSurvey = []
  }
}
