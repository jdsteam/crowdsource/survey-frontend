import { fetchList, fetchPostUpdate, fetchDetail } from '@/api'

export default {
  // list survey
  getListSurvey({ commit }, params) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await fetchList('/api/surveys', 'GET', params)
        commit('SET_LIST_SURVEY', response)
        resolve(response)
      } catch (e) {
        reject(e)
      }
    })
  },
  createSurvey({ commit }, body) {
    return new Promise(async(resolve, reject) => {
      try {
        delete body['id']
        delete body['url_survey_token']
        const response = await fetchPostUpdate('/api/surveys', 'POST', body)
        commit('SET_DATA_SURVEY', response.data)
        resolve(response)
      } catch (e) {
        reject(e)
      }
    })
  },
  updateSurvey({ commit }, body) {
    return new Promise(async(resolve, reject) => {
      try {
        const id_survey = body.id
        delete body['id']
        delete body['url_survey_token']
        const response = await fetchPostUpdate(`/api/surveys/${id_survey}`, 'PUT', body)
        commit('SET_DATA_SURVEY', response.data)
        resolve(response)
      } catch (e) {
        reject(e)
      }
    })
  },
  detailSurvey({ commit }, id) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await fetchDetail('/api/surveys', 'GET', id)
        commit('SET_DATA_SURVEY', response.data)
        commit('SET_DETAIL_SURVEY', response.data)
        resolve(response)
      } catch (e) {
        reject(e)
      }
    })
  },
  deleteSurvey({ commit }, body) {
    return new Promise(async(resolve, reject) => {
      try {
        const id_survey = body.id
        delete body['id']
        const response = await fetchPostUpdate(`/api/surveys/${id_survey}/delete`, 'DELETE', body)
        resolve(response)
      } catch (e) {
        reject(e)
      }
    })
  },
  createQuestionsSurvey({ commit }, data) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await fetchPostUpdate(`/api/surveys/${data.id_survey}/questions`, 'POST', data.questions)
        commit('SET_QUESTIONS_SURVEY', response.data)
        resolve(response)
      } catch (e) {
        reject(e)
      }
    })
  },
  getQuestionsBySurvey({ commit }, id_survey) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await fetchList(`/api/surveys/${id_survey}/questions`, 'GET')
        commit('SET_QUESTIONS_SURVEY', response.data.questions)
        resolve(response)
      } catch (e) {
        reject(e)
      }
    })
  },
  getSurveyByKey({ commit }, key) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await fetchList(`/api/surveys/${key}/form`, 'GET')
        commit('SET_DETAIL_SURVEY', response.data)
        resolve(response)
      } catch (e) {
        reject(e)
      }
    })
  },
  getResponseSurvey({ commit }, params) {
    return new Promise(async(resolve, reject) => {
      try {
        const response = await fetchList(`/api/answers/${params.id_survey}`, 'GET', params)
        commit('SET_LIST_RESPONSE_SURVEY', response.data)
        resolve(response)
      } catch (e) {
        reject(e)
      }
    })
  },
  setQuestionSurvey({ commit }, data) {
    commit('PUSH_QUESTIONS_ARRAY_SURVEY', data)
  },
  setFormSurveyDescription({ commit }, data) {
    commit('SET_FORM_SURVEY_DESCRIPTION', data)
  },
  setFormSurveyIntroduction({ commit }, data) {
    commit('SET_FORM_SURVEY_INTRODUCTION', data)
  },
  deleteQuestionSurvey({ commit }, index) {
    commit('DELETE_QUESTIONS_ARRAY_SURVEY', index)
  },
  resetFormSurvey({ commit }) {
    commit('RESET_FORM_SURVEY')
  },
  resetDetailSurvey({ commit }) {
    commit('RESET_DETAIL_SURVEY')
  },
  resetListSurvey({ commit }) {
    commit('RESET_LIST_SURVEY')
  }
}
