export default {
  listSurvey: (state, getters) => {
    return state.listSurvey
  },
  listResponseSurvey: (state, getters) => {
    console.log(state.listResponseSurvey)
    return state.listResponseSurvey
  },
  formSurvey: (state, getters) => {
    return state.formSurvey
  },
  questionsList: (state, getters) => {
    return state.questionsList
  },
  totalPage: (state, getters) => {
    return state.totalPage
  },
  detailSurvey: (state, getters) => {
    return state.detailSurvey
  }
}
