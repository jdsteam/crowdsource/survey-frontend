export default {
  roles: (state, getters) => {
    return state.roles
  },
  token: (state, getters) => {
    return state.token
  }
}
