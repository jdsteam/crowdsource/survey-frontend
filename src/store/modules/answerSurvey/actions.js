import { fetchPostUpdate } from '@/api'

export default {
  saveAnswerSurvey({ commit }, data) {
    return new Promise((resolve, reject) => {
      fetchPostUpdate(`/api/answers/${data.key_survey}`, 'POST', data.answer).then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
    })
  }
}
