import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vue from 'vue'
import store from '@/store'

import FormDescription from '@/components/Survey/FormDescription'
Vue.use(Vuetify)
import i18n from '@/lang'
const localVue = createLocalVue()
localVue.use(Vuetify)
localVue.use(i18n)

describe('FormDescription.vue', () => {
  it('should render FormDescription', () => {
    const props = {
      formSurvey: {
        description: '',
        introduction: ''
      }
    }
    shallowMount(FormDescription, {
      localVue,
      store,
      propsData: props,
      i18n
    })
  })
})
