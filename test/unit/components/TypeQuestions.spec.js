import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vue from 'vue'

import Address from '@/components/TypeQuestions/Address'
import CheckBoxQuestion from '@/components/TypeQuestions/CheckBoxQuestion'
import FillOutQuestion from '@/components/TypeQuestions/FillOutQuestion'
import RadioButton from '@/components/TypeQuestions/RadioButton'

import DistrictCity from '@/components/SelectRegion/DistrictCity'
import SubDistrict from '@/components/SelectRegion/SubDistrict'
import Village from '@/components/SelectRegion/Village'

Vue.use(Vuetify)
const localVue = createLocalVue()
import i18n from '@/lang'

Vue.component('select-area-district-city', DistrictCity)
Vue.component('select-area-sub-district', SubDistrict)
Vue.component('select-area-village', Village)
localVue.use(Vuetify)

const props = {
  question: {
    description: ''
  }
}

describe('Type Questions', () => {
  it('should render Address', () => {
    shallowMount(Address, {
      localVue,
      i18n
    })
  })
  it('should render Checkbox', () => {
    shallowMount(CheckBoxQuestion, {
      localVue,
      i18n
    })
  })
  it('should render FillOutQuestion', () => {
    shallowMount(FillOutQuestion, {
      localVue,
      propsData: props,
      i18n
    })
  })
  it('should render RadioButton', () => {
    shallowMount(RadioButton, {
      localVue,
      i18n
    })
  })
})
