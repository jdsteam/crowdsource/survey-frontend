import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import vuetify from '@/utils/vuetify'
import Router from 'vue-router'
import infiniteScroll from 'vue-infinite-scroll'

import Vue from 'vue'

import SurveyFormDescription from '@/components/Survey/FormDescription'
import SurveyFormQuestion from '@/components/Survey/FormQuestion'
import SurveyQuestionsViews from '@/components/Survey/QuestionsViews'
import SurveyList from '@/components/Survey/SurveyList'
import Search from '@/components/Search'
import Pagination from '@/components/Pagination'
import Tinymce from '@/components/Tinymce'
import FormSurvey from '@/views/survey/FormAnswer'
import ListSurvey from '@/views/survey/ListSurvey'
Vue.component('survey-form-description', SurveyFormDescription)
Vue.component('survey-form-question', SurveyFormQuestion)
Vue.component('survey-questions-views', SurveyQuestionsViews)
Vue.component('search', Search)
Vue.component('pagination', Pagination)
Vue.component('tinymce', Tinymce)
Vue.component('survey-list', SurveyList)
Vue.use(infiniteScroll)
const router = new Router()
import store from '@/store'
import i18n from '@/lang'
const localVue = createLocalVue()
localVue.use(vuetify)
localVue.use(i18n)

const props = {
  name_survey: '',
  questions: [],
  answers: null,
  key_survey: ''
}

describe('Survey', () => {
  it('should render form survey', () => {
    shallowMount(FormSurvey, {
      localVue,
      propsData: props,
      vuetify,
      store,
      router,
      i18n
    })
  })
  it('should render list survey', () => {
    mount(ListSurvey, {
      localVue,
      vuetify,
      store,
      router,
      i18n
    })
  })
})
